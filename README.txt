TAXONOMY MAPPINGS
-- SUMMARY --
Taxonomy term creation UI,
Let users create taxonomy individually based on vocabulary check.

 -- REQUIREMENTS --
Date, Date Popup (https://www.drupal.org/project/date).

 -- INSTALLATION --

1. Install as usual, 
see http://drupal.org/documentation/install/modules-themes/modules-7 for 
further information.
2. After successfully installing, you will see taxonomy mapping page under
 '/admin/taxonomy/mappings'.
3.Choose the vocabulary and create New term with respective fields.

Note : Supporting Field Type.

1. Boolean (Widget : Check boxes / radio buttons)
2. Date (Widget : Pop-up calendar)
3. Image (Widget : Image)
4. Long text (Widget : Text area (multiple rows))
5. Long text and Summary (Widget : Text area with a summary)
6. List Float (Widget : Text Field)
7. List Integer (Widget : Check boxes / radio buttons)
8. List Text (Widget : Select, Check boxes / radio buttons)
9. Term reference (Widget : Select, Check boxes / radio buttons)
10. Text (Widget : Text Field)

@TODO :: Working on Other field types. 
