<?php

/**
 * @file
 * Allows users to new term based on vocabulary check.
 */

/**
 * Implements hook_menu().
 */
function taxonomy_mappings_page_form($form, &$form_state) {
  drupal_add_library('system', 'ui.datepicker');
  $vocabulary = taxonomy_mappings_get_taxonomy_vocabulary_list();
  reset($vocabulary);
  $mapping_vocabulary_id = !empty($form_state['values']['mappings']) ? $form_state['values']['mappings'] : key($vocabulary);

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'taxonomy_mappings') . '/css/taxonomy_mappings.css',
  );

  $form['mappings'] = array(
    '#title' => t('Choose Vocabulary'),
    '#type' => 'radios',
    '#options' => $vocabulary,
    '#default_value' => $mapping_vocabulary_id,
    '#attributes' => array('class' => array('auto_submit')),
    '#ajax' => array(
      'callback' => 'taxonomy_mappings_ajax_callback_vocabulary_fields',
      'wrapper' => 'vocabulary-fields',
      'method' => 'replace',
    ),
  );
  $form['vocabulary_fieldset'] = array(
    '#title' => taxonomy_mappings_get_vocabulary_name($mapping_vocabulary_id),
    '#prefix' => '<div id="vocabulary-fields">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
  );
  $form['vocabulary_fieldset']['name'] = array(
    '#title' => t("Title"),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['vocabulary_fieldset']['description'] = array(
    '#title' => t("Description"),
    '#type' => 'textarea',
  );

  $vocabulary_machine_name = taxonomy_mappings_get_vocabulary_machine_name($mapping_vocabulary_id);
  if (!empty($vocabulary_machine_name)) {
    $field = field_info_instances('taxonomy_term', $vocabulary_machine_name);
    if (!empty($field)) {
      foreach ($field as $keys => $data) {
        $field_details = field_info_field($keys);
        $type = $data['widget']['type'];
        $type_explode = explode('_', $data['widget']['type']);
        $type_explode[1] = $type_explode[1] == 'image' ? 'managed_file' : ($type == 'date_popup' ? $type : ($type_explode[1] == "buttons" ? 'radios' : $type_explode[1]));
        $form['vocabulary_fieldset']['fields_' . $keys] = array(
          '#title' => ucwords($data['label']),
          '#type' => $type_explode[1],
          '#id' => 'edit-field-' . $keys,
          '#required' => $data['required'],
        );
        $type_data_explode = explode("_", $field_details['type']);
        if ($field_details['type'] == 'taxonomy_term_reference') {
          $term_ref = $field_details['settings']['allowed_values'][0]['vocabulary'];
          $optios[$keys] = taxonomy_mappings_get_taxonomy_vocabulary($term_ref);
          $form['vocabulary_fieldset']['fields_' . $keys]['#options'] = $optios[$keys];
        }
        elseif ($field_details['type'] == 'datetime') {
          $form['vocabulary_fieldset']['fields_' . $keys]['#date_format'] = 'd-m-Y';
        }
        elseif ($type_data_explode[0] == 'list') {
          $options[$keys] = $field_details['settings']['allowed_values'];
          $form['vocabulary_fieldset']['fields_' . $keys]['#options'] = $options[$keys];
        }
        elseif ($field_details['type'] == 'image') {
          $description = t('Files must be less than !size. Allowed file types: png gif jpg jpeg.', array('!size' => '<strong>' . format_size(ini_get('upload_max_filesize')) . '</strong>'));
          $form['vocabulary_fieldset']['fields_' . $keys]['#description'] = $description;
          $form['vocabulary_fieldset']['fields_' . $keys]['#upload_location'] = "public://mappings/images";
        }
      }
    }
  }
  $form['submit'] = array(
    '#prefix' => '<div class="submit-button">',
    '#value' => t("submit"),
    '#type' => 'submit',
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Implements Form submit.
 */
function taxonomy_mappings_page_form_submit($form, &$form_state) {
  if (!empty($form_state['values'])) {
    $vid = $form_state['values']['mappings'];
    $name = $form_state['values']['name'];
    $new_term = array(
      'name' => $name,
      'vid' => $vid,
      'description' => !empty($form_state['values']['description']) ? $form_state['values']['description'] : '',
    );
    $data = field_info_instances('taxonomy_term', taxonomy_mappings_get_vocabulary_machine_name($vid));
    if (!empty($data)) {
      foreach ($data as $keys => $value) {
        $field_data = field_info_field($keys);
        reset($field_data);
        $value_key = key($field_data['columns']);
        $new_term[$keys] = array(
          LANGUAGE_NONE => array(array($value_key => $form_state['values']['fields_' . $keys])),
        );
      }
    }
    $term_obj = (object) $new_term;
    taxonomy_term_save($term_obj);
    drupal_set_message(t('Created new term %name.', array('%name' => $name)));
  }
}

/**
 * Implements #Ajax callback.
 */
function taxonomy_mappings_ajax_callback_vocabulary_fields($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  return $form['vocabulary_fieldset'];
}
